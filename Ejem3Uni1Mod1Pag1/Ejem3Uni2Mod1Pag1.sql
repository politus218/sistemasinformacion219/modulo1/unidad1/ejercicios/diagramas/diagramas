﻿-- Ejemplo3 unidad1 módulo1 página1

DROP DATABASE b20190618;
CREATE DATABASE  IF NOT EXISTS b20190618;
USE b20190618;

CREATE TABLE clientes (
  idcliente int,
  nombrecliente varchar (100),
  PRIMARY KEY (idcliente)
  );

CREATE TABLE productos (
  idproducto int,
  mombreproducto varchar (100),
  peso varchar (10),
  PRIMARY KEY (idproducto)
  );
CREATE TABLE compran (
  idproducto int,
  idcliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY (idproducto, idcliente),
  UNIQUE KEY (idcliente, idproducto),
  CONSTRAINT fkcompraidprod FOREIGN KEY (idproducto) REFERENCES productos (idproducto),
  CONSTRAINT fkidclicompran FOREIGN KEY (idcliente) REFERENCES clientes (idcliente) 
  );